<?php

namespace Backtheweb\Google\TagManager;

class DataLayer
{
    /**
     * @var array
     */
    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Add data to the data layer. Supports dot notation.
     * Inspired by laravel's config repository class.
     *
     * @param array|string $key
     * @param mixed        $value
     */
    public function push($key, $value = null)
    {
        if (is_array($key)) {

            foreach ($key as $innerKey => $innerValue) {
                $this->data[$innerKey] = $innerValue;
            }

            return;
        }

        $this->data[$key] = $value;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null){

        return isset( $this->data[$key] ) ? $this->data[$key] : $default;
    }

    public function exist($key)
    {
        return isset($this->data[$key]) ? true : false;
    }

    /**
     * Empty the data layer.
     */
    public function clear()
    {
        $this->data = [];
    }

    /**
     * Return an array representation of the data layer.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * Return a json representation of the data layer.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->data, JSON_UNESCAPED_UNICODE);
    }
}