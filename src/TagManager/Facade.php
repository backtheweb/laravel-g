<?php

namespace Backtheweb\Google\TagManager;

class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'tagmanager';
    }
}
