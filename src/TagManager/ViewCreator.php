<?php

namespace Backtheweb\Google\TagManager;

use Illuminate\View\View;

class ViewCreator
{
    public function create(View $view)
    {
        $view
            ->with('id',        app('tagmanager') ->id)
            ->with('dataLayer', app('tagmanager')->getDataLayer() )
        ;
    }
}
