<?php

namespace Backtheweb\Google;

use Backtheweb\Google\TagManager\DataLayer;

class TagManager
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var DataLayer
     */
    static $dataLayer;

    /**
     * TagManager constructor.
     * @param $id
     * @param array $dataLayer
     */
    public function __construct($id, Array $dataLayer = [])
    {
        $this->id             = $id;

        if(!self::$dataLayer){
            self::$dataLayer      = new DataLayer($dataLayer);
        }
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Push data to datalayer
     *
     * @param $key
     * @param null $value
     * @return $this
     */
    public function push($key, $value = null)
    {
        self::$dataLayer->push($key, $value);

        return $this;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key){

        return self::$dataLayer->get($key);
    }

    /**
     * @return DataLayer
     */
    public function getDataLayer()
    {
        return self::$dataLayer;
    }

    /*** DL seters and getters ***/

    /**
     * @param $value
     * @return $this
     */
    public function setDomain($value)
    {
        $this->push('domain', $value);
        return $this;
    }

    /**
     * @param $key
     * @param array $arguments
     * @return mixed|null
     */
    public function __call($key, array $arguments)
    {
        return self::$dataLayer->get($key);
    }

    /**
     * @param array $data
     * @return string
     */
    public function query(array $data = [])
    {
        return http_build_query(

            array_merge( self::$dataLayer->toArray(), $data )
        );
    }

    /**
     * @param array $data
     * @return string
     */
    public function toScript(array $data = []) : string
    {
        foreach ($data as  $key => $value){

            self::$dataLayer->push($key, $value);
        }

        return view('tagmanager::head');
    }
}
