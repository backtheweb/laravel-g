<?php

namespace Backtheweb\Google;

use Illuminate\Support\ServiceProvider;

use Google\Client;

class GoogleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindif(Client::class, function () {
            return $this->app['google'];
        });

        $this->app->bindif('google', function($app) {

            /** @var \Illuminate\Foundation\Application $app */

            $client = new Client();
            $client->setApplicationName($app->config->get('services.google.app_name'));
            $client->setDeveloperKey($app->config->get('services.google.api_key_ip'));

            return $client;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
