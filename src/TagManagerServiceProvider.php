<?php

namespace Backtheweb\Google;

use Illuminate\Support\ServiceProvider;

/**
 *
 *
 * php artisan vendor:publish --provider="Backtheweb\Google\TagManagerServiceProvider" --tag="config"
 *
 * Class BackthewebServiceProvider
 * @package Backtheweb\Geo
 */

class TagManagerServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([        __DIR__ . '/../config/dataLayer.php'                => config_path('datalayer.php')],                       'config');
        $this->publishes([        __DIR__ . '/TagManager/resources/views/tagManager'  => base_path(  'resources/views/vendor/tagManager') ],   'views');
        $this->loadViewsFrom(__DIR__ . '/TagManager/resources/views/tagManager', 'tagmanager');

        $this->app['view']->creator([

                'tagmanager::head',
                'tagmanager::noscript',
            ],

            'Backtheweb\Google\TagManager\ViewCreator'
        );
    }

    /**
     * Register the application services.
     *s
     * @return void
     */
    public function register()
    {
        $this->app->bindif(TagManager::class, function ($app) {
            return $this->app['tagmanager'];
        });

        $this->app->bindif('tagmanager', function($app) {
            return new TagManager( config('services.google.tagmanager'), config('datalayer') ?? [] );
        });
    }

    public function provides()
    {
        return ['tagmanager'];
    }
}
