<?php

return  [

    'site'   => env('APP_NAME',  'laravel'),
    'debug'  => env('APP_DEBUG',  false),
    'locale' => env('locale'),
];
